#!/usr/bin/make -f
# Made with the aid of debmake, by Christoph Lameter,
# based on the sample debian/rules file for GNU hello by Ian Jackson.

CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

CFLAGS += " -D_REENTRANT"
CFLAGS += $CPPFLAGS

SHELL=/bin/bash

include /usr/share/dpkg/architecture.mk

ifeq ($(origin CC),default)
CC = $(DEB_HOST_GNU_TYPE)-gcc
endif
export CC

i = $(shell pwd)/debian/tmp
b = $(shell pwd)/debian/build

package = rplay
soname = 3
l = lib$(package)$(soname)

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp

build-stamp:
	dh_testdir

	./configure --prefix=/usr --sysconfdir=/etc/rplay --without-x \
		--enable-rplayd-user=nobody --enable-rplayd-group=audio

	#XXX $(MAKE) CFLAGS="-O2 -D_REENTRANT"
	$(MAKE)

	#XXX (cd contrib/mailsound; $(MAKE) CFLAGS="-O2 -g")
	(cd contrib/mailsound; $(MAKE))

	touch build-stamp

clean:
	dh_testdir
	dh_testroot

	rm -f build-stamp

	[ ! -f Makefile ] || $(MAKE) distclean

	rm -f contrib/mailsound/mailsound{,.o} \
		gsm/Makefile rx/{config.*,Makefile}

	dh_clean

install:
install: build
	dh_testdir
	dh_testroot
	dh_prep

	$(MAKE) install prefix=$(i)/usr \
		infodir=$(i)/usr/share/info \
		mandir=$(i)/usr/share/man

        # Lintian overrides
	cd debian ; \
	for f in *.lintian ; do \
	  [ -r "$$f" ] && { \
	    mkdir -p "$${f%%.lintian}/usr/share/lintian/overrides" ;\
	    install  -m 644 "$$f" "$${f%%.lintian}/usr/share/lintian/overrides/$${f%%.lintian}" ;\
	  } ;\
	done

	install contrib/mailsound/mailsound $(i)/usr/bin
	mkdir -p $(i)/usr/share/perl5
	install -m 664 perl/{RPlay.pm,rplay.ph,RPTP.pm} $(i)/usr/share/perl5
	install perl/Mailsound $(i)/usr/bin
	install -m 644 contrib/mailsound/mailsound.1 $(i)/usr/share/man/man1
	install -m 644 perl/Mailsound.1 $(i)/usr/share/man/man1
	install -d $(i)/etc/rplay
	install -m 644 etc/rplay.{conf,helpers,hosts,servers} $(i)/etc/rplay

	dh_movefiles

binary: binary-indep binary-arch

binary-indep: build install
	dh_installdocs -i
	dh_installexamples -i
	dh_installmenu -i
	dh_installman -i
	dh_installinfo -i
	dh_installchangelogs -i	ChangeLog
	dh_strip -i
	dh_link -i
	dh_compress -i
	dh_fixperms -i
	dh_makeshlibs -i
	dh_installdeb -i
	dh_perl -i
	dh_shlibdeps -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installdocs -a
	dh_installexamples -a
	dh_installmenu -a
	dh_installinit -a		-prplay-server --init-script=rplay -- defaults 99 10
	dh_installman -a
	dh_installinfo -a
	dh_installchangelogs -a	ChangeLog
	dh_strip -a
	dh_link -a
	dh_compress -a
	dh_fixperms -a
	dh_makeshlibs -a
	dh_installdeb -a
	dh_perl -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
